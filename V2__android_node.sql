INSERT INTO user (id, pseudo, email, password, birthdate, createdAt, updatedAt) VALUES
(1, 'Dada', 'dada@gmail.com', '$2a$10$i6NwGmU9musxqbTstrYfveK7.QsmI5typXbLFy/d/Q2OArsbLmXpW', '1998-11-09', '2021-01-09', '2021-01-09');

INSERT INTO project (id, title, picture, description, montant, end_date, idUser, createdAt, updatedAt) VALUES
(2, 'Voiture à l\'huile', '', 'Une voiture qui fonctionne à l\'huile de friture !', 99999, '2025-11-01', 1, '2021-01-09', '2021-01-09'),
(3, 'Ballon de foot connecté', '', 'Un ballon de football qui enregistre la trajectoire/vitesse/force de frappe.', 12547, '2022-02-10', 1, '2021-01-09', '2021-01-09'),
(4, 'Tracteur modulable', '', 'Un tracteur transformers !', 667, '2021-11-01', 1, '2021-01-09', '2021-01-09'),
(14, 'IA inventrice de projet', 'ed20d5fb96eb37b7bbdde0f422f2562e.jpg', 'Une IA qui invente des projets pour cette appli.', 450000, '2021-01-19', 1, '2021-01-10', '2021-01-10'),
(15, 'Chien robot', '4217d1175a53c252935c3ff6478adab8.jpg', 'Un chien robot trop mimi !', 55555, '2024-01-24', 1, '2021-01-10', '2021-01-10'),
(16, 'Pomme de terre parlante', 'cb988eb600e6339b823fc57fbc006b15.jpg', 'Un vrai Mr Patate !', 2222, '2021-02-24', 1, '2021-01-10', '2021-01-10');

INSERT INTO dons (id, idProject, idUser, montant, createdAt, updatedAt) VALUES
(5, 4, 1, 122, '2021-01-10', '2021-01-10'),
(6, 4, 1, 126, '2021-01-10', '2021-01-10'),
(7, 4, 1, 999, '2021-01-10', '2021-01-10'),
(8, 4, 1, 22222, '2021-01-10', '2021-01-10'),
(9, 4, 1, 555, '2021-01-10', '2021-01-10'),
(10, 2, 1, 100, '2021-01-10', '2021-01-10'),
(11, 4, 1, 1, '2021-01-10', '2021-01-10'),
(12, 3, 1, 25, '2021-01-06', '2021-01-06'),
(13, 15, 1, 57, '2021-01-01', '2021-01-01'),
(14, 14, 1, 7, '2021-01-02', '2021-01-02'),
(15, 16, 1, 80, '2021-01-07', '2021-01-07');