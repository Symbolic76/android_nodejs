# Android_Nodejs

# Explication   
Ce projet est une api destinée à l'application [AndroidCrowdFunding](https://github.com/SatarAs/AndroidCrowdFunding) 

# Outils utilisés
- ExpressJS: Routing
- Sequelize: Un ORM pour les requetes avec la base de données
- Body-Parser: Pour gérer les requetes HTTP post
- Multer: Pour gérer l'upload de fichiers
- MYSQL2 : Pour utiliser une base données mysql

# Installation
### Clonez le projet sur votre machine locale
- ```git clone https://gitlab.com/Symbolic76/android_nodejs.git```

### Installez les dépendances
- ```npm install```

### Mettez à jour les infos de base de données
- Dans le fichier server.js modifiez les valeurs à votre convenance : 
``` 
const sequelize = new Sequelize('android_node', 'root', '', {
  host: 'localhost',
  port: 3306,
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
})
```
### Installez flyway
- Téléchargez Flyway
- Dans le fichier flyway.conf (dans le dossier conf du dossier flyway téléchargé), modifiez les valeurs en fonction de votre base de donnée :
``` 
flyway.url=jdbc:mysql://localhost:8889/android_node
flyway.user=root
flyway.password=root
```

### Effectuez les migrations
- Ouvrez un terminal au dossier de Flyway
- Lancez la commande : flyway migrate

### Lancez le projet
START: ``` npm start ```
