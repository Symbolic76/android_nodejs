const express = require("express");
const bodyParser = require("body-parser");
const multer = require('multer');
const path = require('path');
const crypto = require('crypto');
var multiparty = require('multiparty');
const { check, validationResult } = require('express-validator');
const { matchedData, sanitize } = require('express-validator/filter') 
const bcrypt = require ('bcryptjs');
const Sequelize = require('sequelize');
const { type } = require("os");
const { NOW } = require("sequelize");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

const uploadDir = "/img/";

// Multer upload directory config
const storage = multer.diskStorage({
  destination: "./public"+uploadDir,
  filename: function(req,file,cb){
    crypto.pseudoRandomBytes(16,function(err,raw) {
      if(err){
        return cb(err);
      }
      cb(null, raw.toString('hex') + path.extname(file.originalname))
    })
  }
})

const upload = multer({storage: storage, dest: uploadDir});

const port = 3000;
const base = "http://localhost:"+port;

// Database Connection
const sequelize = new Sequelize('android_node', 'damien', '', {
  host: 'localhost',
  port: 3306,
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
})

// User Model
const user = sequelize.define('user', {
  'id': {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  'email': Sequelize.STRING,
  'password': Sequelize.STRING,
  'pseudo': Sequelize.STRING,
  'birthdate': Sequelize.DATE,
  'createdAt': {
    type: Sequelize.DATE,
    defaultValue: Sequelize.NOW
  },
  'updatedAt': {
    type: Sequelize.DATE,
    defaultValue: Sequelize.NOW
  },
},
  {
    freezeTableName: true
  }
)

// Project model
const project = sequelize.define('project', {
  'id': {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  'idUser': Sequelize.INTEGER,
  'title': Sequelize.STRING,
  'description': Sequelize.STRING,
  'montant': Sequelize.INTEGER,
  'end_date':Sequelize.DATE,
  'picture': {
    type: Sequelize.STRING,
    get(){
      const image = this.getDataValue('picture');
      return uploadDir+image;
    }
  },
  'createdAt': {
    type: Sequelize.DATE,
    defaultValue: Sequelize.NOW
  },
  'updatedAt': {
    type: Sequelize.DATE,
    defaultValue: Sequelize.NOW
  }
},
  {
    freezeTableName: true
  }
)

// Dons model
const dons = sequelize.define('dons', {
  'id':{
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  'idUser': {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  'idProject': {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  'montant': Sequelize.INTEGER,
  'createdAt': {
    type: Sequelize.DATE,
    defaultValue: NOW
  },
  'updatedAt': {
    type: Sequelize.DATE,
    defaultValue: Sequelize.NOW
  },

},
  {
    freezeTableName: true,
  }
)

/**
 * Routing
 */

app.get('/project/', (req, res) =>{
  project.findAll().then(project => {
    res.json(project)
  })
})

app.post('/project/add', [
  upload.single('image'),

  check('title')
    .isLength({min: 6}),
    check('montant')
      .isNumeric(),
], (req, res) => {
  const errors = validationResult(req);
  if(!errors.isEmpty()){
    return res.status(200).json({status: 'Error during request', message: "The form is not valid", data: null, errors: errors.mapped()  });

  }

  project.create({
    title: req.body.title,
    montant: req.body.montant,
    end_date: req.body.end_date,
    idUser: req.body.idUser,
    description: req.body.description,
    picture: req.file === undefined ? "" : req.file.filename
  }).then(newProject => {
    res.json({
      "status":"Ok",
      "message":"Project was added",
      "data": newProject
    })
  })

})


app.post('/register', express.static('public'), (req,res) => {
  var form = new multiparty.Form();
  form.parse(req, function(err, fields, files) {
    console.log(fields);
    var password;
      bcrypt.hash(fields.password[0], 10, (err, hash) => {
        console.log(err);
        if(err) throw err;
        password = hash;
        user.create({
          email: fields.email[0],
          password: password,
          pseudo: fields.pseudo[0],
          birthdate: fields.birthdate[0]
        }).then(newUser => {
          res.json({
            "status": "Ok",
            "message": "User added",
            "data": newUser
          });
        });
      });
    });
});

app.post('/login', (req, res) => {
  var form = new multiparty.Form();
  form.parse(req, function(err, fields, files) {
    user.findOne({where: {email: fields.email}}).then(user => {
      console.log(user.password);
      console.log(fields.password)
      bcrypt.compare(fields.password[0], user.password)
        .then(function (result) {
          console.log(result);
          if(result === true) {
            console.log("Result ok");
            res.json({
              "status": "Ok",
              "message": "User logged in",
              "data": user
            })
          } else {
            return res.json({"status": "Not ok",
              "message": "Wrong credentials"
            })
          }
        })
    })
  })
})


app.post('/dons', (req, res) => {
  var form = new multiparty.Form();
  form.parse(req, function(err, fields, files) {
    console.log(fields)
    dons.create({
      idUser: parseInt(fields.idUser[0]),
      idProject: parseInt(fields.idProject[0]),
      montant: parseInt(fields.montant[0]),
    }).then(newDons => {
      res.json({
        "status": "ok",
        "message": "Dons added",
        "data": newDons
      })
    })
  })
})

app.get('/project/:id', (req,res) => {
  const sql = "SELECT * FROM project WHERE id = "+req.params.id;
  sequelize.query(sql, {
    type: sequelize.QueryTypes.SELECT
  }).then(singleProject => {
    console.log(singleProject);
    res.json(singleProject[0]);
  })
})
app.get('/find/dons/:id', (req, res) => {
  const sql = "SELECT SUM(montant) as TOTAL_COST FROM dons WHERE idProject = "+req.params.id+" GROUP BY idProject";
  sequelize.query(sql, {
    type: sequelize.QueryTypes.SELECT
  }).then(dons => {
    console.log(dons);
    if(dons[0] == undefined){
      dons[0] = {TOTAL_COSTS:'0'};
    }
      
    res.json(dons[0]);
    console.log(dons[0]);
  })
})
app.get("/", (req, res) => {
  res.json({ message: "Accueil" });
});


app.listen(port, () => {
  console.log("Server is running on "+base);
});