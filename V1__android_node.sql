create table dons(
  id int(11) NOT NULL,
  idProject int(11) NOT NULL,
  idUser int(11) NOT NULL,
  montant int(11) NOT NULL,
  createdAt date NOT NULL,
  updatedAt date NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table project(
  id int(11) NOT NULL,
  title varchar(130) NOT NULL,
  picture varchar(255) NOT NULL,
  description longtext NOT NULL,
  montant int(11) NOT NULL,
  end_date date NOT NULL,
  idUser int(11) NOT NULL,
  createdAt date NOT NULL,
  updatedAt date NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table user(
  id int(11) NOT NULL,
  pseudo varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  birthdate date DEFAULT NULL,
  createdAt date NOT NULL,
  updatedAt date NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE dons CHANGE id id INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE project CHANGE id id INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE user CHANGE id id INT(11) NOT NULL AUTO_INCREMENT; 

ALTER TABLE dons
  ADD CONSTRAINT fk_Funding_Project1 FOREIGN KEY (idProject) REFERENCES project (id),
  ADD CONSTRAINT fk_Funding_User1 FOREIGN KEY (idUser) REFERENCES user (id);

ALTER TABLE project
  ADD CONSTRAINT fk_Project_User FOREIGN KEY (idUser) REFERENCES user (id);